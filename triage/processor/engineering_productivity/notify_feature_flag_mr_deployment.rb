# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  class NotifyFeatureFlagMrDeployment < Processor
    react_to 'merge_request.update'

    def applicable?
      event.from_gitlab_org_gitlab? &&
        feature_flag_label? &&
        environment_label_added?
    end

    def process
      notify_feature_flag_mr_deployment
    end

    def documentation
      <<~TEXT
        This processor reminds the MR author to enable the feature flag when their ~"feature flag" MR has ~"workflow::<environment>" label added.
      TEXT
    end

    private

    def feature_flag_label?
      event.label_names.include?(Labels::FEATURE_FLAG_LABEL)
    end

    def environment_label_added?
      !!environment_label_added
    end

    def environment_label_added
      (event.added_label_names & Labels::WORKFLOW_ENVIRONMENTS).first
    end

    def notify_feature_flag_mr_deployment
      comment = <<~MARKDOWN.chomp
        :rocket: @#{event.resource_author.username} This merge request was deployed to the ~"#{environment_label_added}" environment. You may want to enable the associated feature flag on this environment.
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
