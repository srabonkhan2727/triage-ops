# frozen_string_literal: true
require_relative '../../triage/sucker_punch'
require_relative '../../triage'
require_relative '../../triage/processor'

module Triage
  class MonitoringEventProcessor < Processor
    react_to 'monitoring.uptime_check'

    def documentation
      "Responding to uptime check events sent by google monitoring. This event is configured at \n
      https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/3fec3f66ce6f9e6d0f58ce15cbb3720b86226dab/qa-resources/modules/triage-reactive/main.tf#L109"
    end

    def process
      SuckerPunch.logger.info('Triage-ops uptime check passed.')
    end
  end
end
