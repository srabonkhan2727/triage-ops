# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/changed_file_list'

module Triage
  class CeoChiefOfStaffTeamOpsNotifier < Processor
    COST_OPS_PATHS = [
      'handbook/ceo/chief-of-staff-team/_index.md'
    ].freeze

    COST_OPS_CODEOWNERS_PATHS = [
      '.gitlab/CODEOWNERS'
    ].freeze

    LABELS_FOR_FILE = {
      '.gitlab/CODEOWNERS' =>
        ['CoST', 'handbook::content'].freeze,
      'handbook/ceo/chief-of-staff-team/_index.md' =>
        ['CoST', 'handbook::content'].freeze
    }.freeze

    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      event.from_www_gitlab_com_or_handbook? &&
        (cost_ops_related_change? || cost_ops_codeowners_related_change?) &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(review_request_comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor requests DRIs to review merge requests when changes are done to pages related to the Chief of Staff Team to the CEO
      TEXT
    end

    private

    def cost_ops_related_change?
      changed_file_list.any_change?(COST_OPS_PATHS)
    end

    def cost_ops_codeowners_related_change?
      changed_file_list.any_change?(COST_OPS_CODEOWNERS_PATHS)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def review_request_comment
      comment = if cost_ops_codeowners_related_change?
                  <<~MARKDOWN.chomp
                    Please tag `@gitlab-com/ceo-chief-of-staff-team` to review this MR as it modifies a `.gitlab/CODEOWNERS` that they maintain.

                    #{labels}
                  MARKDOWN
                else
                  <<~MARKDOWN.chomp
                    Please tag `@gitlab-com/ceo-chief-of-staff-team` when you are ready for this MR to be reviewed as it modifies files related to the [CoST](https://handbook.gitlab.com/handbook/ceo/chief-of-staff-team/).

                    #{labels}
                  MARKDOWN
                end

      unique_comment.wrap(comment).strip
    end

    def labels
      labels = changed_file_list.merge_request_changes.flat_map do |change|
        labels_for_change(change)
      end.uniq

      return '' if labels.empty?

      "/label #{labels.map { |label| "~\"#{label}\"" }.join(' ')}"
    end

    def labels_for_change(change)
      Triage::ChangedFileList::OLD_NEW_PATHS.flat_map do |old_or_new|
        path = change[old_or_new]
        LABELS_FOR_FILE[path] || []
      end
    end
  end
end
