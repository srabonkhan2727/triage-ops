# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/scheduled_issue_type_label_nudger_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ScheduledIssueTypeLabelNudgerJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      { from_part_of_product_project?: true,
        milestone_id: milestone_id }
    end
  end

  let(:milestone_id)         { 123 }
  let(:issue)                { Triage::Issue.new(issue_attr) }
  let(:issue_notes_api_path) { "/projects/#{project_id}/issues/#{iid}/notes" }

  let(:issue_attr) do
    { 'project_id' => project_id, 'iid' => iid, 'labels' => label_names }
  end

  let(:username) { 'root' }
  let(:resource_milestone_event_attr) do
    {
      user: {
        username: username,
        name: "Administrator"
      },
      milestone: {
        id: milestone_id
      },
      action: "add",
      state: "opened"
    }
  end

  let(:issue_validator) { subject.__send__(:validator) }

  let(:comment_request_body) do
    add_automation_suffix('job/scheduled_issue_type_label_nudger_job.rb') do
      <<~MARKDOWN.chomp
        #{issue_validator.type_label_unique_comment.__send__(:hidden_comment)}
        :wave: @#{username} - please see the following guidance and update this issue.
        | | 1 Error |
        | -------- | -------- |
        | :x: | #{described_class::TYPE_LABEL_MISSING_MESSAGE} |

        #{described_class::TYPE_IGNORE_FOOTER_MESSAGE}
      MARKDOWN
    end
  end

  let(:resource_milestone_events_path) { "/projects/#{project_id}/issues/#{iid}/resource_milestone_events" }
  let(:stubbed_paginated_link) { "#{Triage.api_endpoint(:com)}/paginated_link" }
  let(:first_link_in_response_header) { "<#{stubbed_paginated_link}>; rel=\"first\"" }
  let(:last_link_in_response_header) { "<#{stubbed_paginated_link}>; rel=\"last\"" }

  describe '#perform' do
    before do
      stub_api_request(
        path: "/projects/#{project_id}/issues/#{iid}",
        response_body: issue_attr
      )

      stub_api_request(
        path: "/projects/#{project_id}/issues/#{iid}/resource_milestone_events",
        query: { per_page: 1 },
        response_headers: { 'Link' => "#{first_link_in_response_header}, #{last_link_in_response_header}" },
        response_body: [resource_milestone_event_attr]
      )

      stub_api_request(
        path: "/paginated_link",
        response_body: [resource_milestone_event_attr]
      )
    end

    context 'when issue does not require type label nudge' do
      before do
        allow(issue_validator).to receive(:type_label_nudge_needed?).and_return(false)
      end

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when issue requires type label nudge' do
      before do
        allow(issue_validator).to receive(:type_label_nudge_needed?).and_return(true)
      end

      it 'posts a reminder comment to the issue and pings username who last updated the milestone' do
        expect_api_request(
          verb: :post,
          request_body: { body: comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end
  end
end
