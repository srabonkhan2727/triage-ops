# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/processor/broken_master_label_nudger'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BrokenMasterLabelNudger do
  include_context 'with event', Triage::IncidentEvent do
    let(:is_master_broken_incident_project) { true }
    let(:gitlab_bot_event_actor) { false }
    let(:event_attrs) do
      { from_master_broken_incidents_project?: is_master_broken_incident_project,
        gitlab_bot_event_actor?: gitlab_bot_event_actor,
        label_names: label_names }
    end

    let(:label_names) { ['master:broken', 'master-broken::undetermined'] }
    let(:issue_path)  { "/projects/#{project_id}/issues/#{iid}" }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['incident.close']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event is not from master-broken-incident project' do
      let(:is_master_broken_incident_project) { false }

      include_examples 'event is not applicable'
    end

    context 'when event actor is gitlab-bot' do
      let(:gitlab_bot_event_actor) { true }

      include_examples 'event is not applicable'
    end

    context 'when event has no master:broken or master:foss-broken label' do
      let(:label_names) { ['master-broken::undetermined'] }

      include_examples 'event is not applicable'
    end

    context 'when event has master:foss-broken label' do
      let(:label_names) { ['master:foss-broken', 'master-broken::undetermined'] }

      include_examples 'event is applicable'
    end

    context 'when incident has a concrete root cause label' do
      let(:label_names) { ['master-broken::infrastructure'] }

      include_examples 'event is not applicable'
    end

    context 'when a flaky test incident has no concrete root cause label' do
      let(:label_names) { ['master:broken', 'master-broken::flaky-test'] }

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:issue_notes_api_path) { "#{issue_path}/notes" }

    context 'when missing root cause label' do
      it 'nudges for root cause label' do
        body = <<~MARKDOWN.chomp
        <!-- triage-serverless BrokenMasterLabelNudger -->
        :wave: @root, #{described_class::ROOT_CAUSE_LABEL_MISSING_MESSAGE} following the [Triage DRI Responsibilities handbook page](https://about.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities).

        Ignore this comment if you think the incident already has all of the necessary labels to conclude your root cause analysis.

        *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/broken_master_label_nudger.rb).*
        MARKDOWN

        expect_api_request(
          verb: :post,
          request_body: { body: body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.process
        end
      end
    end

    context 'when missing flaky test label' do
      let(:label_names) { ['master:broken', 'master-broken::flaky-test'] }

      it 'nudges for flaky test label' do
        body = <<~MARKDOWN.chomp
        <!-- triage-serverless BrokenMasterLabelNudger -->
        :wave: @root, #{described_class::FLAKY_REASON_LABEL_MISSING_MESSAGE} following the [Triage DRI Responsibilities handbook page](https://about.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities).

        Ignore this comment if you think the incident already has all of the necessary labels to conclude your root cause analysis.

        *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/broken_master_label_nudger.rb).*
        MARKDOWN

        expect_api_request(
          verb: :post,
          request_body: { body: body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.process
        end
      end
    end
  end
end
