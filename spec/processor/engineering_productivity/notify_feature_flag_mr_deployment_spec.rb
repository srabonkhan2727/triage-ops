# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/notify_feature_flag_mr_deployment'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::NotifyFeatureFlagMrDeployment do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:env_label) { 'workflow::staging-canary' }
    let(:added_label_names) { [env_label] }
    let(:label_names) { ['feature flag'] + added_label_names }
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: true,
        label_names: label_names,
        added_label_names: added_label_names
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when MR does not have the "feature flag" label' do
      let(:label_names) { added_label_names }

      include_examples 'event is not applicable'
    end

    context 'when MR does not have environment labels added' do
      let(:added_label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message' do
      body = add_automation_suffix('processor/engineering_productivity/notify_feature_flag_mr_deployment.rb') do
        <<~MARKDOWN.chomp
          :rocket: @joe This merge request was deployed to the ~"#{env_label}" environment. You may want to enable the associated feature flag on this environment.
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
