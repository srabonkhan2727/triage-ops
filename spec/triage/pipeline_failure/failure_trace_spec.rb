# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/failure_trace'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'
require_relative '../../../lib/devops_labels'

GITLAB_PROJECT_ID = Triage::Event::GITLAB_PROJECT_ID
FIXTURE_PATH      = '/reactive/job_traces'

RSpec.describe Triage::PipelineFailure::FailureTrace do
  let(:event) { instance_double(Triage::PipelineEvent, id: 123, project_id: GITLAB_PROJECT_ID) }
  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:failed_examples_data) do
    [
      { "id" => "./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]", "status" => "failed", "feature_category" => "team_planning", "product_group_label" => "group::project management", "feature_category_label" => "Category:Team Planning" },
      { "id" => "./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]", "status" => "failed", "feature_category" => "team_planning", "product_group_label" => "group::project management", "feature_category_label" => "Category:Team Planning" }
    ]
  end

  let(:backend_trace)       { read_fixture("#{FIXTURE_PATH}/rspec_failure.txt") }
  let(:frontend_trace)      { read_fixture("#{FIXTURE_PATH}/jest_failure.txt") }
  let(:workhorse_job_trace) { read_fixture("#{FIXTURE_PATH}/workhorse-test.txt") }

  subject do
    described_class.new(
      job_name: job_name,
      job_trace: job_trace,
      config: config,
      failed_examples_data: failed_examples_data)
  end

  context 'with frontend trace' do
    let(:job_name) { 'jest1' }
    let(:job_trace) { frontend_trace }

    it 'adds ~frontend label' do
      expect(subject.summary_markdown).to eq(
        <<~MARKDOWN.chomp
        ```javascript
        FAIL spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js
          ● vue_shared/component/markdown/markdown_editor › disabled › disables content editor when disabled prop is true

            : Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Error:

              148 |     });
              149 |
            > 150 |     it('disables content editor when disabled prop is true', async () => {
                  |     ^
              151 |       buildWrapper({ propsData: { disabled: true } });
              152 |
              153 |       await enableContentEditor();

              at new Spec (node_modules/jest-jasmine2/build/jasmine/Spec.js:124:22)
              at Suite.it (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:150:5)
              at Suite.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:137:3)
              at Object.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:19:1)


        Test Suites: 1 failed, 532 passed, 533 total
        Tests:       1 failed, 2 skipped, 32 todo, 7782 passed, 7817 total
        Snapshots:   56 passed, 56 total
        Time:        1332.608 s
        ```

        /label ~frontend
        MARKDOWN
      )
    end
  end

  context 'with backend trace' do
    let(:job_name) { 'rspec1' }
    let(:job_trace) { backend_trace }

    it 'adds ~backend label for backend trace' do
      expect(subject.summary_markdown).to eq(
        <<~MARKDOWN.chomp
        ```ruby

          1) Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
             Got 0 failures and 2 other errors:
             Shared Example Group: "Set to auto-merge activator" called from ./spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb:161

             1.1) Failure/Error: super

                  Capybara::ElementNotFound:
                    Unable to find button "Set to auto-merge" that is not disabled

                    Timeout (45s) reached while running a waiting Capybara finder.
                  # ------------------
                  # --- Caused by: ---
                  # Capybara::ElementNotFound:
                  #   Unable to find button "Set to auto-merge" that is not disabled
                  #   ./spec/support/capybara_slow_finder.rb:18:in `synchronize'

        Finished in 2 minutes 6.7 seconds (files took 1 minute 2.1 seconds to load)
        1 example, 1 failure

        Failed examples:

        rspec './spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb[1:2:1:3:1:1]' # Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
        ```

        /label ~backend
        MARKDOWN
      )
    end
  end

  context 'with workhorse trace' do
    let(:job_name) { 'workhorse:test go' }
    let(:job_trace) { workhorse_job_trace }

    it 'adds workhorse label to workhorse specs' do
      expect(subject.summary_markdown).to eq(
        <<~MARKDOWN.chomp
        ```go

        fatal: No names found, cannot describe anything.
        mkdir -p testdata/scratch
        ### Building gitlab-resize-image
        go build -ldflags "-X main.Version=v16.2.0-pre -X main.BuildTime=20230621.181106" -tags "tracer_static tracer_static_jaeger continuous_profiler_stackdriver fips" -o /builds/gitlab-org/gitlab/workhorse/gitlab-resize-image gitlab.com/gitlab-org/gitlab/workhorse/cmd/gitlab-resize-image
        server response: not found: temporarily unavailable
        ```

        /label ~workhorse
        MARKDOWN
      )
    end
  end

  describe 'attribution message' do
    let(:job_name)  { 'rspec1' }
    let(:job_trace) { backend_trace }

    context 'with rspec test and valid feature_categories' do
      it 'builds the correct attribution message' do
        expect(subject.attribution_message_markdown).to eq(
          <<~MARKDOWN.chomp
            - ~"group::project management" ~"Category:Team Planning" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
            - ~"group::project management" ~"Category:Team Planning" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]
          MARKDOWN
        )
      end
    end

    context 'with rspec test and missing feature category' do
      let(:failed_examples_data) do
        [
          { "id" => "./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]", "status" => "failed", "feature_category" => "team_planning", "product_group_label" => DevopsLabels::MISSING_PRODUCT_GROUP_LABEL, "feature_category_label" => DevopsLabels::MISSING_FEATURE_CATEGORY_LABEL }
        ]
      end

      it 'builds the correct attribution message' do
        expect(subject.attribution_message_markdown).to eq(
          <<~MARKDOWN.chomp
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
          MARKDOWN
        )
      end
    end

    context 'with workhorse test' do
      let(:job_name) { 'workhorse:test go' }
      let(:job_trace) { workhorse_job_trace }
      let(:failed_examples_data) { [] }

      it 'builds the correct attribution message and labels group::source code' do
        expect(subject.attribution_message_markdown).to eq(
          <<~MARKDOWN.chomp
            - ~"group::source code" workhorse:test go
          MARKDOWN
        )
      end
    end
  end
end
